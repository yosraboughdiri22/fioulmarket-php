test-dev
========

Un stagiaire à créer le code contenu dans le fichier src/Controller/Home.php

Celui permet de récupérer des urls via un flux RSS ou un appel à l’API NewsApi. 
Celles ci sont filtrées (si contient une image) et dé doublonnées. 
Enfin, il faut récupérer une image sur chacune de ces pages.

Le lead dev n'est pas très satisfait du résultat, il va falloir améliorer le code.

Pratique : 
1. Revoir complètement la conception du code (découper le code afin de pouvoir ajouter de nouveaux flux simplement) 

Questions théoriques : 
1. Que mettriez-vous en place afin d'améliorer les temps de réponses du script
- Mettre système de cache
- Utiliser des appels asynchrone
2. Comment aborderiez-vous le fait de rendre scalable le script (plusieurs milliers de sources et images)
- Parallélisation des requêtes : utilisez des bibliothèques ou des fonctionnalités de Symfony permettant d'effectuer des requêtes en parallèle. Par exemple, utilisez des fonctions asynchrones ou des tâches en arrière-plan pour traiter plusieurs requêtes simultanément.
- Mise en cache des résultats
- Utilisation d'une file d'attente
- Mise en place d'une architecture scalable
- Mettez en place un système de surveillance pour suivre les performances de votre application en temps réel.
