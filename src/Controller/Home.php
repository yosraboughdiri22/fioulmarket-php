<?php

namespace App\Controller;


use App\Services\ParserFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Home extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request, ParserFactory $parserFactory)
    {
        $urls = $parserFactory->getUrls(
            [
                'https://newsapi.org/v2/',
                'https://www.commitstrip.com/en/feed/'
            ]
        );
        return $this->render('default/index.html.twig', array('images' => $urls));
    }


}