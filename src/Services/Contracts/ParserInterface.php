<?php

namespace App\Services\Contracts;

interface ParserInterface
{
    public function parse();

    public function supports(string $url): bool;
}