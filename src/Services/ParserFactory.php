<?php

namespace App\Services;

class ParserFactory
{
    /**
     * @var iterable
     */
    private $parsers;

    public function __construct(iterable $parsers)
    {
        $this->parsers = $parsers;
    }

    public function getUrls(array $flux = []): array
    {
        $urls = [];
        foreach ($flux as $url) {
            foreach ($this->parsers as $parser) {
                if($parser->supports($url)) {
                    $urls[] = $parser->parse();
                }
            }
        }
        return array_unique(array_merge(...$urls));
    }
}