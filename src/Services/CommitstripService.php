<?php

namespace App\Services;

use App\Services\Contracts\ParserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CommitstripService implements ParserInterface
{
    /**
     * @var HttpClientInterface
     */
    private $commitstrip;

    public function __construct(HttpClientInterface $commitstrip)
    {
        $this->commitstrip = $commitstrip;
    }

    public function parse(): array
    {
        try {
            $data = $this->commitstrip->request(Request::METHOD_GET, '/en/feed/')->getContent();
            return $this->extractSrc($data);
        } catch (\Throwable $throwable) {
            return [];
        }
    }


    private function extractSrc(string $content)
    {
        $XMLElement = simplexml_load_string($content, 'SimpleXMLElement', LIBXML_NOCDATA);
        $channel = $XMLElement->channel;
        $countItems = count($XMLElement->channel->item);
        $images = [];
        for ($i = 1; $i < $countItems; $i++) {
            $link = (string)$channel->item[$i]->children("content", true);
            $links[$i] = "";
            $doc = new \DOMDocument();
             $doc->loadHTML($link);
            $xpath = new \DOMXPath($doc);
            $xq = $xpath->query('//img[contains(@class,"size-full")]/@src');

            $src = $xq[0]->value;
            if(!preg_match('/(jpg|gif|png|jpeg)$/i', trim($src))) {
                continue;
            }
            $images[] = $src;
        }
        return array_unique($images);
    }



    public function supports(string $url): bool
    {
        return str_contains(parse_url($url)['host'], 'www.commitstrip.com');
    }
}