<?php

namespace App\Services;

use App\Services\Contracts\ParserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NewsapiService implements ParserInterface
{
    /**
     * @var HttpClientInterface
     */
    private $newsapi;

    public function __construct(HttpClientInterface $newsapi)
    {
        $this->newsapi = $newsapi;
    }

    public function parse(): array
    {
        try {

            $data = $this->newsapi->request(Request::METHOD_GET, '/v2/top-headlines')->getContent();
            return $this->extractSrc($data);

        } catch (\Throwable $throwable) {
            return [];
        }

    }

    private function extractSrc(string $content)
    {
        $data = json_decode($content, false);
        $images = [];
        foreach ($data->articles as $article) {
            $doc = new \DomDocument();
            @$doc->loadHTMLFile($article->url);
            $xpath = new \DomXpath($doc);
            $xq = $xpath->query('//img/@src');

            if (count($xq) && ($src = $xq[0]->value) && preg_match('/(jpg|gif|png|jpeg)$/i', trim($src))) {
                $images[] = $src;
            }
        }
        return array_unique($images);
    }

    public function supports(string $url): bool
    {
        return str_contains(parse_url($url)['host'], 'newsapi.org');
    }
}